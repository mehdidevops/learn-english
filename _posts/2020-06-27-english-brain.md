---
title: English Brain!
date: 2020-06-27
sub_heading: thinking, speaking , writing english conversations 
tags:
- English Brain 
layout: post
banner_image: ''
---
Hello my name is Mehdi
Are you OK
If you don't know me I'm an English trainer from Channapatna.
And welcome to this course on building your English brain.

Now i want to in this very first video explain what the course will be about.
I want to introduce the course to you.
So let me do that in English brain what I call the English brain is ability to think in English when you're speaking in English.
Very Simple.
If you want to be a good English speaker you must have the ability to think in English.
And this seems obvious.
Yes of course.
But how to do that is difficult part right how to begin tho think in English.
So in this course we're going to talk about a lot of different ways that we can improve this ability to think in English.
If you're thinking in your language not in English you won't be able to sound natural you won't be able to sound really fluent and so in order to do this we must learn these skills.
So in this course I'm going to be first teaching you some general tips things you can do generally to improve and practice this English brain thinking in English.
OK.
And then we'll go on from that.
We'll talk about reading why reading is really important and how to read, the right way to read and what kinds of things to read to improve this English brain and kind reading must be part of your English exercise English practice English progress.

OK the next part is we'll talk about vocabulary learning words how we learn words  vocabulary.
I'll be writing on the board like this.
Vocabulary simply means words OK the right way to learn words.
Very important to learn words in the right way.
There's a right way and wrong way.
We'll talk about that then we'll talk about idioms
idioms our little expressions that don't change and we'll talk about when to use them when not to use them.
I'm not goint to teach you too many.
I'm going to teach you the way to learn them so you won't be learning.
Many many words or idioms or long pieces of literature in this course and you won't be learning that you will be learning methods.

The ways to study English to improve your English brain.
They then will go on after that and we'll talk about some specific exercises practise that you can do which I've developed for my students to help them really really improve their English more quickly.

I one is called the word exercise or the four word challenge.
and this is a really great and very simple thing that you can do to really start thinking in English a lot.
Ok
we'll go on from that.
we'll do variations variations is another really important and usefull tool that you can use to improve your thinking ability in English.
So we'll talk about that with practice it will do a few examples of that and then we'll go on to listening how we can do listening as a way to improve the English brain and we'll do some examples of that from listening.
We'll go on the right way to think things that you can do during the day to always be improving 
how to beign to actually just think without speaking without reading without writing hate to actually think and we'll do some small writing exercises as well in the course and then we'll talk about how to use different resources  resources means what you have to help you learn.

This course is a resource for you to improve your English especially your ability to thinking in English.

Well there are other resources your vocabulary books maybe your dictionary might be a resource.
Well movies the things you read TV shows these things are also resources and we'll talk about how to use these resources.

To best develop your English brain.
Talk about movies , tv, Internet and how to use these things to practice and improve.
Then I'll give you a daily routine.
I'll give you a routine for what you can do every day.
And you really only need to spend generally between 30 minutes and an hour every day to improve generally improve.
And it can help you improve your fluency her ability to think in English and generally your ability to communicate naturally in English.

So I'll give you this routine and I think that can really really help you if you do it you will see improvement you will improve over time.
You must you must.
Ok.
So I hope you enjoy this course.
I hope you can make it to the end because there's a lot of good stuff on the way.
And I look forward to seeing you in the next lesson we're going to just be going over some simple tips a few things to keep in mind when you're actually studying English and generally for improving your English skills.


